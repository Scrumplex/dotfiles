dotfiles
--------
Collection of my configuration files. Managed with [GNU Stow](https://www.gnu.org/software/stow/).

# License
All files in this repository are licensed under the terms of the GNU General Public License 3.0 or newer, **unless otherwise stated**.

Read the license text in [LICENSE](LICENSE)
